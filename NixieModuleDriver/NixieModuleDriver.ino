#include <Wire.h>

// I2C interface
#define I2C_VALUE                     0x00      // The value we want to display
#define I2C_DIM                       0x01      // dim value, 10 = 10%, 100 = 100%
#define I2C_FADE                      0x02      // Use fade stunt, 0 = no, > 0 = fade steps
#define I2C_SCROLL                    0x03      // Use scroll stunt, 0 = no, > 0 = scroll steps
#define I2C_RGB_BL                    0x04      // RGB backlight value 0..255 for each channel
#define I2C_DP                        0x05      // Set the DP, 0 = no, > 0 = yes
#define I2C_SELF_TEST                 0x06      // Set the self test, 0 = no, > 0 = yes
#define I2C_SAVE                      0x07      // Save the dim,fade,scroll values into EEPROM
#define I2C_IDENTIFY                  0x08      // Address assgignment: identify this module 
#define I2C_ASSIGN_ADDR               0x09      // Address assgignment: set a permanent address from a identification address
#define I2C_SHOW_ADDR                 0x10      // Address assgignment: show the address we have
#define I2C_BL_CYCLE_SPEED            0x11      // Backlight cycling, 0 = off, > 0 = speed

#define I2C_SLAVE_ADDR   0x33

byte currentAddress = 0;

byte digitValue = 0;
byte dimValue = 0;

void setup() {
  Wire.begin();
  Serial.begin(115200);
  printMenu();
}

void loop() {
  if (Serial.available() > 0) {
    char command = Serial.read();

    Serial.print("Received command: ");
    Serial.println(command);

    if (command == 'A') {
      Serial.println("Input module address to communicate with (0..255)");
      Serial.println("(255 is 'Broadcast')");
      byte addressValue = getByteValue();
      Serial.print("Setting address to: ");
      Serial.println(addressValue);
      currentAddress = addressValue;
      printMenu();
    }

    if (command == 'C') {
      Serial.println("Cycling:");
      cycleNumbersTwice();
      printMenu();
    }

    if (command == 'O') {
      Serial.println("cycle fOrever:");
      cycleNumbersForever();
      printMenu();
    }

    if (command == 'S') {
      Serial.println("Input digit value (0..9)");
      byte digitValue = getByteValue();
      Serial.print("Setting digit to: ");
      Serial.println(digitValue);
      setDigitValue(digitValue);
      printMenu();
    }

    if (command == 'B') {
      Serial.println("Input red value (0..255)");
      byte redValue = getByteValue();
      Serial.println("Input green value (0..255)");
      byte greenValue = getByteValue();
      Serial.println("Input blue value (0..255)");
      byte blueValue = getByteValue();
      Serial.print("Setting red to: ");
      Serial.print(redValue);
      Serial.print(", Setting green to: ");
      Serial.print(greenValue);
      Serial.print(", Setting blue to: ");
      Serial.println(blueValue);
      setBacklightValue(redValue, greenValue, blueValue);
      printMenu();
    }

    if (command == 'D') {
      Serial.println("Input dimming value (10..100)");
      byte digitValue = getByteValue();
      Serial.print("Setting dimming to: ");
      Serial.println(digitValue);
      setDimValue(digitValue);
      printMenu();
    }

    if (command == 'F') {
      Serial.println("Input fade value (0..100)");
      byte fadeValue = getByteValue();
      Serial.print("Setting fade to: ");
      Serial.println(fadeValue);
      setFadeValue(fadeValue);
      printMenu();
    }

    if (command == 'R') {
      Serial.println("Input scroll value (0..50)");
      byte scrollValue = getByteValue();
      Serial.print("Setting scroll to: ");
      Serial.println(scrollValue);
      setScrollValue(scrollValue);
      printMenu();
    }

    if (command == 'P') {
      Serial.println("Input decimal point value 0=off, 1=on)");
      byte dpValue = getByteValue();
      Serial.print("Setting decimal point to: ");
      Serial.println(dpValue);
      setDecimalPointValue(dpValue);
      printMenu();
    }

    if (command == 'I') {
      Serial.println("Identifying modules:");
      identifyModules();
      printMenu();
    }

    if (command == 'W') {
      Serial.println("Show address of modules:");
      showAddress();
      printMenu();
    }

    if (command == 'G') {
      Serial.println("Input identify address (0...254)");
      byte identValue = getByteValue();
      Serial.print("Setting identify address to: ");
      Serial.println(identValue);

      Serial.println("Input module address (0...254)");
      byte addressValue = getByteValue();
      
      Serial.print("Setting module address to: ");
      Serial.println(addressValue);
      
      setIdentifiedModuleAddress(identValue, addressValue);
      
      Serial.println("Remember to set the address with 'A' now!!");
      printMenu();
    }

    if (command == 'V') {
      Serial.println("Saving config");
      saveConfig();
      printMenu();
    }

    if (command == 'T') {
      Serial.println("Self test on startup: 0=off, 1=on)");
      byte stValue = getByteValue();
      Serial.print("Setting self test to: ");
      Serial.println(stValue);
      setSelfTestValue(stValue);
      Serial.println("This setting can only be seen on power on!!");
      printMenu();
    }

    if (command == 'U') {
      Serial.println("Set cycle speed: (0..100)");
      byte cycleSpeedValue = getByteValue();
      Serial.print("Setting backlight cycle speed to: ");
      Serial.println(cycleSpeedValue);
      setBacklightCycle(cycleSpeedValue);
      printMenu();
    }
  }
}

byte getByteValue() {
  Serial.setTimeout(60000);
  int val = Serial.parseInt();
  Serial.println(val);
  return (byte) val;
}

void cycleNumbersTwice() {
  for (int i = 0 ; i < 10 ; i++) {
    setDigitValue(i);
    delay(1000);
  }
  for (int i = 0 ; i < 10 ; i++) {
    setDigitValue(i);
    delay(1000);
  }
  setDigitValue(0);
}

void cycleNumbersForever() {
  while (true) {
    for (int i = 0 ; i < 10 ; i++) {
      setDigitValue(i);
      delay(1000);
    }
  }
}
void setDigitValue(byte digitValue) {
  Wire.beginTransmission(I2C_SLAVE_ADDR);
  Wire.write(I2C_VALUE); // operation
  Wire.write(currentAddress); // addr
  Wire.write(digitValue); // digit
  Wire.endTransmission();
}

void setBacklightValue(byte R, byte G, byte B) {
  Wire.beginTransmission(I2C_SLAVE_ADDR);
  Wire.write(I2C_RGB_BL); // operation
  Wire.write(currentAddress); // addr
  Wire.write(R); // R
  Wire.write(G); // G
  Wire.write(B); // B
  Wire.endTransmission();
}

void setDimValue(byte dimValue) {
  Wire.beginTransmission(I2C_SLAVE_ADDR);
  Wire.write(I2C_DIM); // operation
  Wire.write(currentAddress); // addr
  Wire.write(dimValue);
  Wire.endTransmission();
}

void setFadeValue(byte fadeValue) {
  Wire.beginTransmission(I2C_SLAVE_ADDR);
  Wire.write(I2C_FADE); // operation
  Wire.write(currentAddress); // addr
  Wire.write(fadeValue); // fade steps
  Wire.endTransmission();
}

void setScrollValue(byte scrollValue) {
  Wire.beginTransmission(I2C_SLAVE_ADDR);
  Wire.write(I2C_SCROLL); // operation
  Wire.write(currentAddress); // addr
  Wire.write(scrollValue); // scroll steps
  Wire.endTransmission();
}

void setDecimalPointValue(byte decimalPointValue) {
  Wire.beginTransmission(I2C_SLAVE_ADDR);
  Wire.write(I2C_DP); // operation
  Wire.write(currentAddress); // addr
  Wire.write(decimalPointValue); // decimal point value 0 = off
  Wire.endTransmission();
}

void identifyModules() {
  // identify the module
  Wire.beginTransmission(I2C_SLAVE_ADDR);
  Wire.write(I2C_IDENTIFY); // operation
  Wire.write(0xff); // addr
  Wire.endTransmission();
}

void setIdentifiedModuleAddress(byte identityAddress, byte moduleAddress) {
  Wire.beginTransmission(I2C_SLAVE_ADDR);
  Wire.write(I2C_ASSIGN_ADDR); // operation
  Wire.write(0xff); // addr
  Wire.write(identityAddress); // addr
  Wire.write(moduleAddress); // addr
  Wire.endTransmission();
}

void saveConfig() {
  Wire.beginTransmission(I2C_SLAVE_ADDR);
  Wire.write(I2C_SAVE); // operation
  Wire.write(currentAddress); // addr
  Wire.endTransmission();
}

void setSelfTestValue(byte selfTestValue) {
  Wire.beginTransmission(I2C_SLAVE_ADDR);
  Wire.write(I2C_SELF_TEST); // operation
  Wire.write(currentAddress); // addr
  Wire.write(selfTestValue);
  Wire.endTransmission();
}

void showAddress() {
  Wire.beginTransmission(I2C_SLAVE_ADDR);
  Wire.write(I2C_SHOW_ADDR); // operation
  Wire.write(0xff); // addr
  Wire.endTransmission();
}

void setBacklightCycle(byte cycleSpeed) {
  Wire.beginTransmission(I2C_SLAVE_ADDR);
  Wire.write(I2C_BL_CYCLE_SPEED); // operation
  Wire.write(currentAddress); // addr
  Wire.write(cycleSpeed);
  Wire.endTransmission();
}

void printMenu() {
  Serial.println("------------------------");
  Serial.println("Nixie Module Test Harness");
  Serial.print("Using module address: ");
  Serial.println(currentAddress);
  Serial.println("");
  Serial.println("  A: Set Current Module Address to talk to");
  Serial.println("  S: Set Digit Value");
  Serial.println("  B: set Back light value");
  Serial.println("  D: Set Dimming");
  Serial.println("  F: Set Fade Steps");
  Serial.println("  R: Set scRoll Steps");
  Serial.println("  C: Cycle through 0..9 twice");
  Serial.println("  P: Set decimal point");
  Serial.println("  I: Identify modules (all)");
  Serial.println("  G: assiGn address to identified module (all)");
  Serial.println("  W: shoW module addresses (all)");
  Serial.println("  V: saVe configuration to EEPROM");
  Serial.println("  T: set self Test on startup");
  Serial.println("  U: aUto cycle back light");
  Serial.println("  O: cycle numbers fOrever");
  Serial.println("");
  Serial.println("");
}

