You need to set the fuses before the project will run on the module. See the script "setArduinoFusesA8.sh".

--------------------

Installing the board definition:

Paste 

https://raw.githubusercontent.com/sleemanj/optiboot/master/dists/package_gogo_diy_atmega8_series_index.json

into the space at:

File->Preferences->Additonal Board Manager URLs

Now go to Tools->Boards->Board Manager

Search for "A8" and you should be able to install the defintions.

After this, "Atmega8/A" should appear in the boards list.

----------------

Setting the board manually

Add this to boards.txt

##############################################################

##############################################################
opti8.name=Arduino Optiboot-Atmega8-16
opti8.upload.protocol=arduino
opti8.upload.maximum_size=7680
opti8.upload.speed=115200
opti8.bootloader.low_fuses=0xbf
opti8.bootloader.high_fuses=0xcc
opti8.bootloader.path=optiboot
opti8.bootloader.file=optiboot_atmega8-16.hex
opti8.bootloader.unlock_bits=0x3F
opti8.bootloader.lock_bits=0x0F
opti8.build.mcu=atmega8
opti8.build.f_cpu=8000000L
opti8.build.core=arduino
opti8.build.variant=standard

##############################################################

##############################################################

