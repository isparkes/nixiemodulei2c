echo "Set programmer with ArduinoISP first"

echo "# FACTORY default values:  62 D9 07"
echo "# Internal RC oscillator:  E2 D8 FD"
echo "# Arduino default values:  FF DE 05"

/opt/arduino-1.8/hardware/tools/avr/bin/avrdude -c stk500v1 -C /opt/arduino-1.8/hardware/tools/avr/etc/avrdude.conf -p m8 -P /dev/ttyACM$1 -b 19200 -U hfuse:w:highA8NixieModule.txt:s -U lfuse:w:lowA8NixieModule.txt:s
