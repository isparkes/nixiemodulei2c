// board definition here:
// https://raw.githubusercontent.com/sleemanj/optiboot/master/dists/package_gogo_diy_atmega8_series_index.json
// Settings:
// LTO: enabled
// Bootloader: No
// Processor speed: 8Mhz internal
// Processor version: ATmega8A
#include <Adafruit_NeoPixel.h>
#include <Wire.h>
#include <EEPROM.h>

#define LED_DOUT  11    // PB3 // pin 15

#define IN14

#ifdef IN12
  #define CATH0     3     // PD3 // pin 1
  #define CATH1     5     // PD5 // pin 9
  #define CATH2     6     // PD6 // pin 10
  #define CATH3     7     // PD7 // pin 11
  #define CATH4     8     // PB0 // pin 12
  #define CATH5     9     // PB1 // pin 13
  #define CATH6     10    // PB2 // pin 14
  #define CATH7     0     // PD0 // pin 30
  #define CATH8     1     // PD1 // pin 31
  #define CATH9     2     // PD2 // pin 32
  #define CATHDP    4     // PD4 // pin 2
  #define PD_RES    0x10  // reset all except PD4
  #define PB_RES    0xf8  // reset PB0-PB2
#endif

#ifdef IN14
  #define CATH0     9     // PB1 // pin 13
  #define CATH1     0     // PD0 // pin 30
  #define CATH2     1     // PD1 // pin 31
  #define CATH3     2     // PD2 // pin 32
  #define CATH4     10    // PB2 // pin 14
  #define CATH5     4     // PD4 // pin 2
  #define CATH6     5     // PD5 // pin 9
  #define CATH7     6     // PD6 // pin 10
  #define CATH8     7     // PD7 // pin 11
  #define CATH9     8     // PB0 // pin 12
  #define CATHDP    3     // PD3 // pin 1
  #define PD_RES    0x08  // reset all except PD3
  #define PB_RES    0xf8  // reset PB0-PB2
#endif

// Jumpers - don't have a use for these yet!
#define J1        A0    // PC0 // pin 23
#define J2        A1    // PC1 // pin 24
#define J3        A2    // PC2 // pin 25
#define J4        A3    // PC3 // pin 26

// EEPROM locations
#define EE_ADDR         0   // The address we respond to
#define EE_SELF_TEST    1   // perform self test on startup (0 = no)
#define EE_DIM          2   // startup setting of dim
#define EE_FADE         3   // statup setting of fade
#define EE_SCROLL       4   // startup setting of scroll
#define EE_BL_CYCLE     5   // startup setting of cycle speed
#define EE_R            6   // Red
#define EE_G            7   // Green
#define EE_B            8   // Blue

#define I2C_SLAVE_ADDR   0x33

// I2C interface
#define I2C_VALUE                     0x00      // The value we want to display
#define I2C_DIM                       0x01      // dim value, 10 = 10%, 100 = 100%
#define I2C_FADE                      0x02      // Use fade stunt, 0 = no, > 0 = fade steps
#define I2C_SCROLL                    0x03      // Use scroll stunt, 0 = no, > 0 = scroll steps
#define I2C_RGB_BL                    0x04      // RGB backlight value 0..255 for each channel
#define I2C_DP                        0x05      // Set the DP, 0 = no, > 0 = yes
#define I2C_SELF_TEST                 0x06      // Set the self test, 0 = no, > 0 = yes
#define I2C_SAVE                      0x07      // Save the dim,fade,scroll values into EEPROM
#define I2C_IDENTIFY                  0x08      // Address assignment: identify this module 
#define I2C_ASSIGN_ADDR               0x09      // Address assignment: set a permanent address from a identification address
#define I2C_SHOW_ADDR                 0x10      // Address assignment: show the address we have
#define I2C_BL_CYCLE_SPEED            0x11      // Backlight cycling, 0 = off, > 0 = speed

// Display handling
#define DIGIT_DISPLAY_COUNT   1000                    // The number of times to traverse inner fade loop per digit
#define DIGIT_DISPLAY_ON      0                       // Switch on the digit at the beginning by default
#define DIGIT_DISPLAY_OFF     DIGIT_DISPLAY_COUNT - 1 // Switch off the digit at the end by default
#define DIGIT_DISPLAY_NEVER   -1                      // When we don't want to switch on or off (i.e. blanking)
#define DIGIT_DISPLAY_MIN_DIM 100                     // Minimum dimming value

// This is the address we answer to
byte addr = 0;

// used in address assignment
byte randAddr = 0;

byte cathodes[10] = {CATH0, CATH1, CATH2, CATH3, CATH4, CATH5, CATH6, CATH7, CATH8, CATH9};

byte previousValue = 0;
byte currentValue = 0;

// Start with a full brightness display
int dimValue;
byte fadeSteps;
byte scrollSteps;

int fadeState = 0;
int scrollState = 0;
boolean inScroll = false;

long pressStartTime = 0;
boolean settingsMode = false;
long lastDigitChange = 0;
byte settingDigit = 0;
byte lastDigit = 0xff;

// Auto cycle backlight Strategy 3
int changeSteps = 0;
byte currentColour = 0;
byte cycleCount = 0;
byte cycleSpeed = 0;
byte colours[3];

// set up the NeoPixel library
Adafruit_NeoPixel leds = Adafruit_NeoPixel(1, LED_DOUT, NEO_GRB + NEO_KHZ400);

// the setup function runs once when you press reset or power the board
void setup() {
  // get the addr we are at
  byte tmpAddr = EEPROM.read(EE_ADDR);
  if (tmpAddr == 0xff) {
    // we are not set up, assume addr = 0 for now
    addr = 0;
  } else {
    addr = tmpAddr;
  }

  // Speed up internal clock
  OSCCAL = 255;

  // set pins
  for (byte i = 0; i < 10 ; i++) {
    pinMode(cathodes[i], OUTPUT);
    digitalWrite(cathodes[i], LOW);
  }

  pinMode(CATHDP, OUTPUT);
  digitalWrite(CATHDP, LOW);

  // prepare the inputs, so they don't float around
  pinMode(J1, INPUT);
  digitalWrite(J1, HIGH);
  pinMode(J2, INPUT);
  digitalWrite(J2, HIGH);
  pinMode(J3, INPUT);
  digitalWrite(J3, HIGH);
  pinMode(J4, INPUT);
  digitalWrite(J4, HIGH);

  // Read saved values
  dimValue = normaliseDimValue(EEPROM.read(EE_DIM));
  fadeSteps = normaliseFadeValue(EEPROM.read(EE_FADE));
  scrollSteps = normaliseScrollValue(EEPROM.read(EE_SCROLL));
  cycleSpeed = normaliseScrollValue(EEPROM.read(EE_BL_CYCLE));
  colours[0] = normaliseScrollValue(EEPROM.read(EE_R));
  colours[1] = normaliseScrollValue(EEPROM.read(EE_G));
  colours[2] = normaliseScrollValue(EEPROM.read(EE_B));

  // Set up the LED output
  leds.begin();

  // set up I2C
  Wire.begin(I2C_SLAVE_ADDR);
  Wire.onReceive(receiveEvent);
  //Wire.onRequest(requestEvent);

  if (EEPROM.read(EE_SELF_TEST) != 0) {
    for (int i = 0 ; i < 10 ; i++) {
      setDigitOut(i);
      if ((i % 3) == 0) leds.setPixelColor(0, leds.Color(255, 0, 0));
      if ((i % 3) == 1) leds.setPixelColor(0, leds.Color(0, 255, 0));
      if ((i % 3) == 2) leds.setPixelColor(0, leds.Color(0, 0, 255));
      leds.show();
      delay(200);
    }
  }

  // Reset back lights
  leds.setPixelColor(0, leds.Color(colours[0], colours[1], colours[2]));
  leds.show();
}

// ************************************************************
// ************************* Main Loop ************************
// ************************************************************
void loop() {
  outputDisplay();
  if (cycleSpeed > 0) cycleColours3();
}

// ************************************************************
// Do a single complete display, including any fading and
// dimming requested. Performs the display loop
// DIGIT_DISPLAY_COUNT times for each digit, with no delays.
// This is the heart of the display processing!
// ************************************************************
void outputDisplay() {
  int digitSwitchTime;
  if (currentValue != previousValue) {
    // ------------------ scrollback ------------------
    if ((currentValue == 0) && (scrollSteps > 0)) {
      if (scrollState == 0) {
        // Start the fade
        scrollState = scrollSteps;
        digitSwitchTime = DIGIT_DISPLAY_ON;
      }

      if (scrollState == 1) {
        // finish the fade
        scrollState = 0;
        previousValue = previousValue - 1;
        digitSwitchTime = DIGIT_DISPLAY_ON;
      } else if (scrollState > 1) {
        // Continue the scroll countdown
        scrollState = scrollState - 1;
        digitSwitchTime = DIGIT_DISPLAY_ON;
      }
    } else if (fadeSteps > 0) {
      // ------------------ fade ------------------
      if (fadeState == 0) {
        // Start the fade
        fadeState = fadeSteps;
      } else {
        // continue the fade
        fadeState--;
        if (fadeState == 0) {
          previousValue = currentValue;
        }
      }
      digitSwitchTime = (int) ((float) dimValue / (float)fadeSteps * (float)(fadeSteps - fadeState));
    } else {
      digitSwitchTime = DIGIT_DISPLAY_NEVER;
    }
  } else {
    digitSwitchTime = DIGIT_DISPLAY_NEVER;
  }

  setDigitOut(currentValue);
  for (int timer = 0 ; timer < DIGIT_DISPLAY_COUNT ; timer++) {
    if (timer == digitSwitchTime) {
      setDigitOut(previousValue);
    }

    if (timer == dimValue) {
      setDigitOut(0xff);
    }
  }
}

// ************************************************************
// Set the cathode ouput to the defined value
// ************************************************************
void setDigitOut(byte digit) {
  if (digit != lastDigit) {
    // reset all old cathodes
    PORTD = PORTD & PD_RES;
    PORTB = PORTB & PB_RES;

    //  for (int i = 0 ; i < 10 ; i++) {
    //    digitalWrite(cathodes[i], LOW);
    //  }
    if (digit != 0xff) {
      digitalWrite(cathodes[digit], HIGH);
    }
    lastDigit = digit;
  }
}

//**********************************************************************************
//**********************************************************************************
//*                                 I2C interface                                  *
//**********************************************************************************
//**********************************************************************************

// All commands are in the form
//      Operation:addr:params
// Addr is the address of the tube, or 0xff for all tubes
// Operation            params  description
// I2C_VALUE            1       The value we want to display (0..9)
// I2C_DIM              1       dim value, 10 = 10%, 100 = 100%
// I2C_FADE             1       Use fade stunt, 0 = no, > 0 = fade steps
// I2C_SCROLL           1       Use scroll stunt, 0 = no, > 0 = scroll steps
// I2C_RGB_BL           3       RGB backlight value 0..255 for each channel
// I2C_DP               1       Set the DP, 0 = no, > 0 = yes
// I2C_SELF_TEST        1       Set the self test, 0 = no, > 0 = yes
// I2C_SAVE             0       Save the dim,fade,scroll values into EEPROM
// I2C_IDENTIFY         0       Assign a temporary random address
// I2C_ASSIGN_ADDRESS   2       Assign a fixed address to a temporary random address
// I2C_SHOW_ADDRESS     0       Show the fixed address
// I2C_BL_CYCLE_SPEED   1       Backlight cycling, 0 = off, > 0 = speed

void receiveEvent(int bytes) {
  // the operation tells us what we are getting
  int operation = Wire.read();

  // the address tells us if we have to react to this
  int targetAddr = Wire.read();
  boolean forMe = (targetAddr == addr) || (targetAddr == 0xff);

  if (operation == I2C_VALUE) {
    byte newVal = Wire.read();
    if (forMe) {
      if (newVal == 0xff) {}
      else if (newVal < 0 ) newVal = 0;
      else if (newVal > 9 ) newVal = 9;
      currentValue = newVal;
    }
  }

  // normalise the percentage value we get to the range 100...999
  if (operation == I2C_DIM) {
    byte newDimValue = Wire.read();
    if (forMe) {
      dimValue = normaliseDimValue(newDimValue);
    }
  }

  if (operation == I2C_FADE) {
    byte newFadeSteps = Wire.read();
    if (forMe) {
      fadeSteps = normaliseFadeValue(newFadeSteps);
    }
  }

  if (operation == I2C_SCROLL) {
    byte newScrollSteps = Wire.read();
    if (forMe) {
      scrollSteps = normaliseScrollValue(newScrollSteps);
    }
  }

  if (operation == I2C_RGB_BL) {
    byte newRVal = Wire.read();
    byte newGVal = Wire.read();
    byte newBVal = Wire.read();
    if (forMe) {
      colours[0] = newRVal;
      colours[1] = newGVal;
      colours[2] = newBVal;
      leds.setPixelColor(0, leds.Color(newRVal, newGVal, newBVal));
      leds.show();
    }
  }

  if (operation == I2C_DP) {
    byte newVal = Wire.read();
    if (forMe) {
      if (newVal > 0) {
        digitalWrite(CATHDP, HIGH);
      } else {
        digitalWrite(CATHDP, LOW);
      }
    }
  }

  if (operation == I2C_SELF_TEST) {
    byte newVal = Wire.read();
    if (forMe) {
      EEPROM.write(EE_SELF_TEST, newVal);
      doABlink(2, 0);
    }
  }

  if (operation == I2C_SAVE) {
    byte newVal = Wire.read();
    if (forMe) {
      EEPROM.write(EE_DIM, dimValue / 10);
      EEPROM.write(EE_FADE, fadeSteps);
      EEPROM.write(EE_SCROLL, scrollSteps);
      EEPROM.write(EE_BL_CYCLE, cycleSpeed);
      EEPROM.write(EE_R, colours[0]);
      EEPROM.write(EE_G, colours[1]);
      EEPROM.write(EE_B, colours[2]);
      doABlink(2, 0);
    }
  }

  if (operation == I2C_IDENTIFY) {
    if (forMe) {
      randomSeed(millis());
      randAddr = random(255);
      blinkByte(randAddr);
    }
  }

  if (operation == I2C_ASSIGN_ADDR) {
    if (forMe) {
      byte oldVal = Wire.read();
      byte newVal = Wire.read();
      if (oldVal == randAddr) {
        addr = newVal;
        EEPROM.write(EE_ADDR, addr);
        doABlink(2, 0);
      }
    }
  }

  if (operation == I2C_SHOW_ADDR) {
    if (forMe) {
      blinkByte(addr);
    }
  }
  
  if (operation == I2C_BL_CYCLE_SPEED) {
    byte newVal = Wire.read();
    if (forMe) {
      cycleSpeed = normaliseCycleSpeedValue(newVal);
    }
  }
}

//**********************************************************************************
//**********************************************************************************
//*                               Utility functions                                *
//**********************************************************************************
//**********************************************************************************

// Set the dim value to something we can work with
// Byte in (0..255) int out (100..1000)
int normaliseDimValue(byte inputValue) {
  int newDimValueInt = inputValue * 10;
  if (newDimValueInt < DIGIT_DISPLAY_MIN_DIM) {
    newDimValueInt = DIGIT_DISPLAY_MIN_DIM;
  } else if (newDimValueInt > DIGIT_DISPLAY_COUNT) {
    newDimValueInt = DIGIT_DISPLAY_COUNT;
  }
  return newDimValueInt;
}

// Set the fade value to something we can work with
// Byte in (0..255) byte out (0..100)
byte normaliseFadeValue(byte inputValue) {
  byte newFadeSteps = inputValue;
  if (inputValue > 100) newFadeSteps = 100;
  return newFadeSteps;
}

// Set the cycle speed value to something we can work with
// Byte in (0..255) byte out (0..100), 0 = off
byte normaliseCycleSpeedValue(byte inputValue) {
  byte newCycleSpeed = inputValue;
  if (inputValue > 100) newCycleSpeed = 100;
  return newCycleSpeed;
}

// Set the scroll value to something we can work with
// Byte in (0..255) byte out (0..10)
byte normaliseScrollValue(byte inputValue) {
  byte newScrollSteps = inputValue;
  if (inputValue > 50) newScrollSteps = 50;
  return newScrollSteps;
}

// Blinks the LED according to the following scheme:
// 100s: Red
// 10s: Green
// 1s: Blue
// If and of the digits are 0, the LED blinks white
void blinkByte(byte byteToBlink) {
  byte a0 = byteToBlink / 100;
  byte a1 = (byteToBlink % 100) / 10;
  byte a2 = byteToBlink % 10;

  setDigitOut(a0);
  if (a0 == 0) {
    doAShortWhiteBlink();
  } else {
    doABlink(a0, 0);
  }
  delay(1000);
  setDigitOut(a1);
  if (a1 == 0) {
    doAShortWhiteBlink();
  } else {
    doABlink(a1, 1);
  }
  delay(1000);
  setDigitOut(a2);
  if (a2 == 0) {
    doAShortWhiteBlink();
  } else {
    doABlink(a2, 2);
  }
  delay(1000);
}

// Backlight:
// 0: red
// 1: green
// 2: blue
void doABlink(byte number, byte backlight) {
  for (int i = 0 ; i < number ; i++) {
    if (backlight == 0) {
      leds.setPixelColor(0, leds.Color(255, 0, 0));
    } else if (backlight == 1) {
      leds.setPixelColor(0, leds.Color(0, 255, 0));
    } else {
      leds.setPixelColor(0, leds.Color(0, 0, 255));
    }
    leds.show();
    delay(500);
    leds.setPixelColor(0, leds.Color(0, 0, 0));
    leds.show();
    delay(200);
  }
}

void doAShortWhiteBlink() {
  leds.setPixelColor(0, leds.Color(255, 255, 255));
  leds.show();
  delay(250);
  leds.setPixelColor(0, leds.Color(0, 0, 0));
  leds.show();
  delay(250);
}

// ************************************************************
// Colour cycling 3: one colour dominates
// ************************************************************
void cycleColours3() {
  cycleCount++;
  if (cycleCount > cycleSpeed) {
    cycleCount = 0;

    if (changeSteps == 0) {
      changeSteps = random(256);
      currentColour = random(3);
    }

    changeSteps--;

    switch (currentColour) {
      case 0:
        if (colours[0] < 255) {
          colours[0]++;
          if (colours[1] > 0) {
            colours[1]--;
          }
          if (colours[2] > 0) {
            colours[2]--;
          }
        } else {
          changeSteps = 0;
        }
        break;
      case 1:
        if (colours[1] < 255) {
          colours[1]++;
          if (colours[0] > 0) {
            colours[0]--;
          }
          if (colours[2] > 0) {
            colours[2]--;
          }
        } else {
          changeSteps = 0;
        }
        break;
      case 2:
        if (colours[2] < 255) {
          colours[2]++;
          if (colours[0] > 0) {
            colours[0]--;
          }
          if (colours[1] > 0) {
            colours[1]--;
          }
        } else {
          changeSteps = 0;
        }
        break;
    }

    leds.setPixelColor(0, leds.Color(colours[0], colours[1], colours[2]));
    leds.show();
  }
}

